(tool-bar-mode -1)
(menu-bar-mode 1)
(scroll-bar-mode -1)
(setq-default cursor-type 'bar)
;; (load-theme 'solarized-dark)

(if (eq system-type 'darwin)
    (progn (set-frame-font "Monaco 13")
	   (set-default-font "-apple-Monaco-medium-normal-normal-*-14-*-*-*-m-0-iso10646-1"))
  (set-frame-font "DejaVu Sans Mono 12"))

(setq mouse-wheel-scroll-amount '(1 ((shift) . 1))) ;; one line at a time
(setq mouse-wheel-progressive-speed nil) ;; don't accelerate scrolling
(setq mouse-wheel-follow-mouse 't) ;; scroll window under mouse
(setq scroll-step 1) ;; keyboard scroll one line at a time

(load-theme 'sanityinc-solarized-dark)
(nyan-mode 1)
(nyan-start-animation)
