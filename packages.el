;; melpa repository
(require 'package)
(add-to-list 'package-archives
	     '("melpa" . "http://melpa.milkbox.net/packages/") t)

(package-initialize)

(defvar my-packages '(clojure-mode
		      scala-mode
                      cider
		      ensime
		      nyan-mode
		      neotree
		      auto-complete
		      color-theme-sanityinc-solarized))

(dolist (p my-packages)
  (unless (package-installed-p p)
    (package-install p)))
